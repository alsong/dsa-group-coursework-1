#include <stdio.h>
#include <string.h>
#include <stdlib.h> // for remove()
#include <unistd.h> // for access()

typedef struct
{
    char name[51];
    char dateOfBirth[11];
    char regNumber[7]; // Holds 6 characters + null terminator
    char programCode[5];
    double annualTuition;
} Student;

#define MAX_STUDENTS 100
Student students[MAX_STUDENTS];
int studentCount = 0;

void flushInput()
{
    int c;
    while ((c = getchar()) != '\n' && c != EOF)
        ;
}

void deleteStudent()
{
    char regNum[7];
    printf("Enter registration number to delete: ");
    scanf("%6s", regNum);
    flushInput();
    int found = 0;
    for (int i = 0; i < studentCount; i++)
    {
        if (strcmp(students[i].regNumber, regNum) == 0)
        {
            found = 1;
            // Shift elements to overwrite the deleted student
            for (int j = i; j < studentCount - 1; j++)
            {
                students[j] = students[j + 1];
            }
            studentCount--; // Decrement student count
            printf("Student with registration number %s deleted successfully.\n", regNum);
            break;
        }
    }
    if (!found)
    {
        printf("No student found with registration number %s.\n", regNum);
    }
}

void createStudent()
{
    if (studentCount >= MAX_STUDENTS)
    {
        printf("Database full\n");
        return;
    }
    Student newStudent;
    printf("Enter name: ");
    fgets(newStudent.name, 51, stdin);
    newStudent.name[strcspn(newStudent.name, "\n")] = 0; // Remove newline character

    int isValidDate = 0;
    while (!isValidDate)
    {
        printf("Enter date of birth (YYYY-MM-DD): ");
        fgets(newStudent.dateOfBirth, 11, stdin);
        newStudent.dateOfBirth[strcspn(newStudent.dateOfBirth, "\n")] = 0; // Remove newline character

        if (strlen(newStudent.dateOfBirth) == 10 &&
            newStudent.dateOfBirth[4] == '-' &&
            newStudent.dateOfBirth[7] == '-')
        {
            char *endptr;
            long year = strtol(newStudent.dateOfBirth, &endptr, 10);
            long month = strtol(newStudent.dateOfBirth + 5, &endptr, 10);
            long day = strtol(newStudent.dateOfBirth + 8, &endptr, 10);

            if (year >= 1000 && year <= 9999 &&
                month >= 1 && month <= 12 &&
                day >= 1 && day <= 31)
            {
                isValidDate = 1;
            }
        }
        if (!isValidDate)
        {
            printf("Invalid date format. Please enter in YYYY-MM-DD format.\n");
        }
    }

    char inputRegNumber[10]; // Buffer to hold potential over-sized input for validation
    int isValid = 0;         // Flag to check if the input is valid
    while (!isValid)
    {
        printf("Enter registration number (6 digits): ");
        scanf("%9s", inputRegNumber); // Read up to 9 characters to check for over-length
        flushInput();
        // Check if the input has exactly 6 digits and consists only of digits
        if (strlen(inputRegNumber) == 6 && strspn(inputRegNumber, "0123456789") == 6)
        {
            isValid = 1;
            strncpy(newStudent.regNumber, inputRegNumber, 7); // Copy validated number to struct
                                                              // Check if the registration number already exists
            for (int i = 0; i < studentCount; i++)
            {
                if (strcmp(students[i].regNumber, newStudent.regNumber) == 0)
                {
                    printf("Student with this registration number already exists.\n");
                    return; // Exit the function if registration number exists
                }
            }
        }
        else
        {
            printf("Invalid registration number. Please enter exactly 6 digits.\n");
        }
    }

    isValid = 0;               // Reset validity check for program code
    char inputProgramCode[10]; // Similarly, use a buffer for program code
    while (!isValid)
    {
        printf("Enter program code (up to 4 characters): ");
        scanf("%9s", inputProgramCode); // Read up to 9 characters to check for over-length
        flushInput();
        // Check if the input is up to 4 characters and no more
        if (strlen(inputProgramCode) <= 4)
        {
            isValid = 1;
            strncpy(newStudent.programCode, inputProgramCode, 5); // Copy validated program code to struct
        }
        else
        {
            printf("Invalid program code. Please enter up to 4 characters.\n");
        }
    }

    isValid = 0; // Reset validity check for annual tuition
    while (!isValid)
    {
        printf("Enter annual tuition (positive number): ");
        scanf("%lf", &newStudent.annualTuition);
        flushInput();
        if (newStudent.annualTuition > 0)
        {
            isValid = 1;
        }
        else
        {
            printf("Invalid tuition. Please enter a positive number greater than zero.\n");
        }
    }

    students[studentCount++] = newStudent;
    printf("Student added successfully!\n");
}

void displayStudents()
{
    if (studentCount == 0)
    {
        printf("No students are registered yet.\n");
        return;
    }

    for (int i = 0; i < studentCount; i++)
    {
        printf("Name: %s, Date of Birth: %s, Registration Number: %s, Program Code: %s, Tuition: %.2f\n",
               students[i].name, students[i].dateOfBirth, students[i].regNumber, students[i].programCode, students[i].annualTuition);
    }
}

void searchStudentByRegNumber()
{
    char regNum[7];
    printf("Enter registration number to search: ");
    scanf("%6s", regNum);
    flushInput();
    int found = 0;
    for (int i = 0; i < studentCount; i++)
    {
        if (strcmp(students[i].regNumber, regNum) == 0)
        {
            found = 1;
            printf("Student found:\n");
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dateOfBirth);
            printf("Registration Number: %s\n", students[i].regNumber);
            printf("Program Code: %s\n", students[i].programCode);
            printf("Annual Tuition: %.2f\n", students[i].annualTuition);
            break;
        }
    }
    if (!found)
    {
        printf("No student found with registration number %s.\n", regNum);
    }
}

void sortStudentsByName(int ascending)
{
    // Sort students by name
    for (int i = 0; i < studentCount - 1; i++)
    {
        for (int j = i + 1; j < studentCount; j++)
        {
            int compareResult = strcmp(students[i].name, students[j].name);
            if ((ascending && compareResult > 0) || (!ascending && compareResult < 0))
            {
                Student temp = students[i];
                students[i] = students[j];
                students[j] = temp;
            }
        }
    }

    // Print the sorted list of names
    printf("Sorted list by name (%s order):\n", ascending ? "ascending" : "descending");
    printf("[");
    for (int i = 0; i < studentCount; i++)
    {
        printf("%s", students[i].name);
        if (i < studentCount - 1)
        {
            printf(", ");
        }
    }
    printf("]\n");
}

void sortStudentsByRegNumber(int ascending)
{
    // Sort students by registration number
    for (int i = 0; i < studentCount - 1; i++)
    {
        for (int j = i + 1; j < studentCount; j++)
        {
            int compareResult = strcmp(students[i].regNumber, students[j].regNumber);
            if ((ascending && compareResult > 0) || (!ascending && compareResult < 0))
            {
                Student temp = students[i];
                students[i] = students[j];
                students[j] = temp;
            }
        }
    }

    // Print the sorted list of registration numbers
    printf("Sorted list by registration number (%s order):\n", ascending ? "ascending" : "descending");
    printf("[");
    for (int i = 0; i < studentCount; i++)
    {
        printf("%s", students[i].regNumber);
        if (i < studentCount - 1)
        {
            printf(", ");
        }
    }
    printf("]\n");
}

void exportStudentsToCSV()
{
    // Check if the file exists before attempting to open it
    if (access("students.csv", F_OK) == -1)
    {
        // The file doesn't exist, create it and export data
        FILE *newFile = fopen("students.csv", "w");
        if (newFile == NULL)
        {
            printf("Failed to create new file\n");
            return;
        }
        // Export newly added students to the new file
        for (int i = 0; i < studentCount; i++)
        {
            fprintf(newFile, "%s,%s,%s,%s,%.2lf\n",
                    students[i].name, students[i].dateOfBirth, students[i].regNumber,
                    students[i].programCode, students[i].annualTuition);
        }
        fclose(newFile);
        printf("Data exported successfully.\n");
        return;
    }

    // Open the CSV file for appending
    FILE *existingFile = fopen("students.csv", "a");
    if (existingFile == NULL)
    {
        printf("Failed to open existing file for appending\n");
        return;
    }

    // Export newly added students to the CSV file
    for (int i = 0; i < studentCount; i++)
    {
        // Check if the student's registration number already exists in the file
        FILE *checkFile = fopen("students.csv", "r");
        int found = 0;
        if (checkFile != NULL)
        {
            char line[256];
            while (fgets(line, sizeof(line), checkFile))
            {
                char regNumber[7];
                sscanf(line, "%*[^,],%*[^,],%[^,],%*[^,],%*f", regNumber);
                if (strcmp(students[i].regNumber, regNumber) == 0)
                {
                    found = 1;
                    break;
                }
            }
            fclose(checkFile);
        }
        if (!found)
        {
            fprintf(existingFile, "%s,%s,%s,%s,%.2lf\n",
                    students[i].name, students[i].dateOfBirth, students[i].regNumber,
                    students[i].programCode, students[i].annualTuition);
        }
    }

    // Close the CSV file
    fclose(existingFile);

    printf("Data exported successfully.\n");
}
void sortMenu()
{
    int sortChoice;
    int orderChoice;
    printf("\nSort Menu:\n");
    printf("1. Sort by Name\n");
    printf("2. Sort by Registration Number\n");
    printf("Enter your choice for sorting: ");
    scanf("%d", &sortChoice);
    flushInput();
    printf("Enter sorting order (1: Ascending, 2: Descending): ");
    scanf("%d", &orderChoice);
    flushInput();

    switch (sortChoice)
    {
    case 1:
        sortStudentsByName(orderChoice == 1 ? 1 : 0); // Pass 1 for ascending, 0 for descending
        break;
    case 2:
        sortStudentsByRegNumber(orderChoice == 1 ? 1 : 0); // Pass 1 for ascending, 0 for descending
        break;
    default:
        printf("Invalid choice, returning to main menu.\n");
    }
}

void updateStudent()
{
    char regNum[7];
    printf("Enter registration number to update: ");
    scanf("%6s", regNum);
    flushInput();
    int found = 0;
    for (int i = 0; i < studentCount; i++)
    {
        if (strcmp(students[i].regNumber, regNum) == 0)
        {
            found = 1;
            // Update student information
            printf("Enter new name: ");
            fgets(students[i].name, 51, stdin);
            students[i].name[strcspn(students[i].name, "\n")] = 0; // Remove newline character
            printf("Enter new date of birth (YYYY-MM-DD): ");
            fgets(students[i].dateOfBirth, 11, stdin);
            students[i].dateOfBirth[strcspn(students[i].dateOfBirth, "\n")] = 0; // Remove newline character
            printf("Enter new program code (up to 4 characters): ");
            scanf("%4s", students[i].programCode);
            flushInput();
            printf("Enter new annual tuition (positive number): ");
            scanf("%lf", &students[i].annualTuition);
            flushInput();
            printf("Student information updated successfully.\n");
            break;
        }
    }
    if (!found)
    {
        printf("No student found with registration number %s.\n", regNum);
    }
}

void readStudent()
{
    char regNum[7];
    printf("Enter registration number to read: ");
    scanf("%6s", regNum);
    flushInput();
    int found = 0;
    for (int i = 0; i < studentCount; i++)
    {
        if (strcmp(students[i].regNumber, regNum) == 0)
        {
            found = 1;
            printf("Name: %s\n", students[i].name);
            printf("Date of Birth: %s\n", students[i].dateOfBirth);
            printf("Registration Number: %s\n", students[i].regNumber);
            printf("Program Code: %s\n", students[i].programCode);
            printf("Annual Tuition: %.2f\n", students[i].annualTuition);
            break;
        }
    }
    if (!found)
    {
        printf("No student found with registration number %s.\n", regNum);
    }
}

int main()
{
    int choice;
    do
    {
        printf("\nMain Menu:\n");
        printf("1. Create Student\n");
        printf("2. Display All Students\n");
        printf("3. Search by Registration Number\n");
        printf("4. Sort Students\n");
        printf("5. Export to CSV\n");
        printf("6. Delete Student\n");
        printf("7. Update Student\n");
        printf("8. Read Student\n");
        printf("0. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);
        flushInput();

        switch (choice)
        {
        case 1:
            createStudent();
            break;
        case 2:
            displayStudents();
            break;
        case 3:
            searchStudentByRegNumber();
            break;
        case 4:
            sortMenu(); // Call the sort menu
            break;
        case 5:
            exportStudentsToCSV();
            break;
        case 6:
            deleteStudent();
            break;
        case 7:
            updateStudent();
            break;
        case 8:
            readStudent();
            break;
        case 0:
            printf("Exiting the program.\n");
            break;
        default:
            printf("Invalid choice, please try again.\n");
        }
    } while (choice != 0);

    return 0;
}
