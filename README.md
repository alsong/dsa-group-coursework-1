# Student Management System Group H

This C program is designed to manage a database of student records, offering functionalities such as creating, displaying, searching, sorting, updating, deleting, and exporting student data to a CSV file. The core data structure used to represent a student is a `struct` named `Student`, which encapsulates details such as name, date of birth, registration number, program code, and annual tuition.

## Group Members

- [Oburusule Dustan](https://gitlab.com/alsong)
- [Alupo Tabitha](https://gitlab.com/tabithaalupo5)
- [Opio Isaac](https://gitlab.com/IsaacOpio)
- [Ben Kyeyune](https://gitlab.com/Ben_kyeyune2)
- [Sharif Hasahya](https://gitlab.com/hasahyasharif)

## Data Structure Selection

The choice of data structure, in this case, a combination of a `struct` and a static array, was driven by several considerations:

### Simplicity and Accessibility
- **Static Array of Structs**: The use of a static array (`students[MAX_STUDENTS]`) to store `Student` records provides a straightforward way to manage a fixed number of student records. This approach simplifies access to student data through array indices, making operations like sorting and searching efficient with direct access to any element.

### Memory Management
- **Static Allocation**: By allocating a fixed-size array, the program avoids the complexities of dynamic memory management in C. This decision limits the maximum number of students to `MAX_STUDENTS` (defined as 100), which is a trade-off between flexibility and simplicity. For small to medium-sized datasets, this limitation is acceptable and ensures that the memory footprint is predictable and manageable.

### Struct for Student Records
- **Struct Usage**: The `Student` struct is a logical choice for grouping related data about a student. It allows for the encapsulation of different data types (`char` arrays for text fields and `double` for tuition) in a single entity. This encapsulation makes the code more organized, readable, and easier to maintain.

## Core Functionalities

1. **Create Student**: Prompts the user to enter details for a new student and adds the student to the database if there's space and the registration number is unique.
2. **Display All Students**: Lists all registered students along with their details.
3. **Search by Registration Number**: Allows searching for a student by their registration number.
4. **Sort Students**: Provides options to sort students either by name or registration number, in ascending or descending order.
5. **Export to CSV**: Exports the current list of students to a CSV file, creating a new file if it doesn't exist or appending to it if it does.
6. **Delete Student**: Removes a student from the database based on their registration number.
7. **Update Student**: Updates the details of an existing student, identified by their registration number.
8. **Read Student**: Displays the details of a student, searched by their registration number.

## Considerations and Limitations

- **Fixed Array Size**: The maximum number of students is capped at 100, which may not be suitable for larger datasets.
- **Data Validation**: The program includes basic validation for input formats (e.g., date format, registration number format) but could be expanded for more robust error checking.
- **File Handling**: The export function checks for the existence of the `students.csv` file and handles file operations accordingly. However, the program does not currently support importing student data from a file.

This program demonstrates fundamental C programming concepts, including struct manipulation, array handling, basic file operations, and user input validation, making it a solid foundation for more complex data management systems.

## Code Explanation Videos
Below are the videos explaining the system:
- Video explaining all the code: https://www.youtube.com/watch?v=6t13x-fpbTI
-Video explaining how the whole system works: https://youtu.be/NqFG2vkoO3M

Each group member has contributed to a specific part of the project and recorded a video explaining their code. Below are the links to these videos:

- **Oburusule Dustan** - Sorting of the students by any two fields: [Watch Video](https://youtu.be/NYYWSzfQDBE)
- **Opio Isaac** - Searching a student by regNo: [Watch Video](https://youtu.be/-2MHRH8KnV0)
- **Alupo Tabitha** - ceating a student: VIDEO: https://youtu.be/Aj2fC6x5xE0
- **Kyeyune Ben** https://youtu.be/6J9fkkla1kA
- **Sharif Hasahya** - updating, deleting and reading file https://youtu.be/u8aEPimpQ2U
